#Public subnet creation
resource "aws_subnet" "terra_pub_subnet" {
  vpc_id     = aws_vpc.terra_vpc.id
  cidr_block = var.pub_cidr_block
  availability_zone = var.pub_availability_zone

  tags = {
    Name = "terra_pubsbnt"
  }
}

#Private subnet creation
resource "aws_subnet" "terra_priv_subnet" {
  vpc_id     = aws_vpc.terra_vpc.id
  cidr_block = var.priv_cidr_block
  availability_zone = var.priv_availability_zone

  tags = {
    Name = "terra_privsbnt"
  }
}
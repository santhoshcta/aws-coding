#NAT gateway creation
resource "aws_nat_gateway" "terra_nat" {
  allocation_id = aws_eip.terra_eip.id
  subnet_id     = aws_subnet.terra_pub_subnet.id

  tags = {
    Name = "terra_NAT"
  }

}
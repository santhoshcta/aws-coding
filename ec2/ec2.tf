resource "aws_instance" "ec2" {
    ami           = data.aws_ami.goldenami.id
  instance_type = "t2.micro"
  key_name = "devopskey"
  subnet_id = data.terraform_remote_state.vpc.outputs.pub_sub_id
  security_groups = [aws_security_group.allow_tls.id]
  iam_instance_profile = aws_iam_instance_profile.prac_profile.name

  tags = {
      name = "demo-server123"
  }
}

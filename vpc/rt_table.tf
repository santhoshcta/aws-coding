#Public route table creation
resource "aws_route_table" "terra_pub_rt" {
  vpc_id = aws_vpc.terra_vpc.id


  tags = {
    Name = "terra_pubrt"
  }
}

#Private route table creation
resource "aws_route_table" "terra_priv_rt" {
  vpc_id = aws_vpc.terra_vpc.id

  tags = {
    Name = "terra_privrt"
  }
}

#Association of route table to public subnet
resource "aws_route_table_association" "terra_pub_rt_association" {
  subnet_id      = aws_subnet.terra_pub_subnet.id
  route_table_id = aws_route_table.terra_pub_rt.id
}

#Association of route table to private subnet
resource "aws_route_table_association" "terra_priv_rt_association" {
  subnet_id      = aws_subnet.terra_priv_subnet.id
  route_table_id = aws_route_table.terra_priv_rt.id
}

#Adding routes to the public route table
resource "aws_route" "terra_pub_igw" {
  route_table_id            = aws_route_table.terra_pub_rt.id
  destination_cidr_block    = var.destination_cidr_block
  gateway_id                = aws_internet_gateway.gw.id
}

#Adding routes to the private route table
resource "aws_route" "terra_priv_nat" {
  route_table_id            = aws_route_table.terra_priv_rt.id
  destination_cidr_block    = var.destination_cidr_block
  nat_gateway_id            = aws_nat_gateway.terra_nat.id
}

variable "aws_region" {}
variable "destination_cidr_block" {}
variable "pub_cidr_block" {}
variable "pub_availability_zone" {}
variable "priv_cidr_block" {}
variable "priv_availability_zone" {}
variable "vpc_cidr_block" {}
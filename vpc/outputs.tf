output "vpc_id" {
    value = aws_vpc.terra_vpc.id
}

output "pub_sub_id" {
    value = aws_subnet.terra_pub_subnet.id
}
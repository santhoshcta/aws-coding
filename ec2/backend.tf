terraform {  
    backend "s3" {    
        bucket = "prac-terraform-state"    
        key    = "prac/ec2/terraform.tfstate"    
        region = "us-east-1"  
        dynamodb_table = "terraform_state"
        }
        }

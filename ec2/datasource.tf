#To get the VPC information from statefile stored in s3
data "terraform_remote_state" "vpc" {
  backend = "s3"

  config = {
        bucket = "prac-terraform-state"    
        key    = "prac/vpc/terraform.tfstate"    
        region = "us-east-1"  
    }
  }

#To take AMI id from golde AMI
data "aws_ami" "goldenami" {
  most_recent      = true
  owners           = ["010967013623"]

  filter {
    name   = "name"
    values = ["AMI golden*"]
  }

}

